$(document).ready(function () {
var data = [{"text":"Cover Page","icon":"images/folder.svg","href":"startpage.html","target":"DATA"},{"text":"Servers","icon":"images/folder.svg","href":"Servers\\Servers.html","target":"DATA"},{"text":"servidor mio","icon":"images/folder.svg","href":"Servers\\servidor_mio\\servidor_mio.html","target":"DATA","nodes":[{"text":"Databases","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\Databases.html","target":"DATA","nodes":[{"text":"ejemploprogramacion1","icon":"images/database.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\ejemploprogramacion1.html","target":"DATA","nodes":[{"text":"Procedures","icon":"images/folder.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\Procedures.html","target":"DATA","nodes":[{"text":"ej1v1a","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej1v1a.html","target":"DATA"},{"text":"ej1v1b","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej1v1b.html","target":"DATA"},{"text":"ej1v1c","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej1v1c.html","target":"DATA"},{"text":"ej1v2","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej1v2.html","target":"DATA"},{"text":"ej1v3","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej1v3.html","target":"DATA"},{"text":"ej2v1a","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej2v1a.html","target":"DATA"},{"text":"ej2v1b","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej2v1b.html","target":"DATA"},{"text":"ej2v1c","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej2v1c.html","target":"DATA"},{"text":"ej2v2","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej2v2.html","target":"DATA"},{"text":"ej2v3","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej2v3.html","target":"DATA"},{"text":"ej3","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej3.html","target":"DATA"},{"text":"ej4","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej4.html","target":"DATA"},{"text":"ej5","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej5.html","target":"DATA"},{"text":"ej6","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej6.html","target":"DATA"},{"text":"ej7","icon":"images/procedure.svg","href":"Servers\\servidor_mio\\Databases\\ejemploprogramacion1\\Procedures\\ej7.html","target":"DATA"}]}]}]}]}];
$('#tree').treeview({levels: 3,data: data,enableLinks: true,injectStyle: false,highlightSelected: true,collapseIcon: 'images/tree-node-expanded.svg',expandIcon: 'images/tree-node-collapsed.svg'});
});
var loadEvent = function () {

  $('#btn-expand-nodes').on('click', function (e) {
    $('#tree').treeview('expandAll', { silent: true });
  });
  $('#btn-collapse-nodes').on('click', function (e) {
    $('#tree').treeview('collapseAll', { levels:3, silent: true });
  });
  
  var searchTimeOut;
  $('#input-search').on('input', function() {
    if(searchTimeOut != null)
      clearTimeout(searchTimeOut);
    searchTimeOut = setTimeout(function(){
      var pattern = $('#input-search').val();
      var tree = $('#tree');
      tree.treeview('collapseAll', { levels:3, silent: true });
      var options = { ignoreCase: true, exactMatch: false, revealResults: true };
      var results = tree.treeview('search', [pattern, options]);
    }, 500);
  });
  
  $('#tree').on('nodeSelected', function(event, data) {
    // navigate to link
    window.open (data.href, 'DATA', false)
  });
  // select first node.
  $('#tree').treeview('selectNode', [0, { silent: false }]);
}

if (window.addEventListener) {
  window.addEventListener('load', loadEvent, false);
}
else if (window.attachEvent) {
  window.attachEvent('onload', loadEvent);
}